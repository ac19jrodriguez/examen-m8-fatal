package com.example.examenm8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int dificultad = 0;
    String tema = null;
    String nombre = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button enviar;
        RadioButton easy,medium,hard,sumas,restas,multiDiv;
        RadioGroup rg = findViewById(R.id.tipoPreguntas);
        enviar = findViewById(R.id.enviar);
        easy = findViewById(R.id.easy);
        medium= findViewById(R.id.medium);
        hard = findViewById(R.id.hard);
        sumas = findViewById(R.id.sumas);
        restas = findViewById(R.id.restas);
        multiDiv = findViewById(R.id.multiDiv);
        TextView tv = findViewById(R.id.nombre);



        final Intent i = new Intent(MainActivity.this,Activity2.class);


        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if ( easy.isChecked()) {
                    dificultad = 5;
                }else if(medium.isChecked()){
                    dificultad = 10;
                }else if (hard.isChecked()){
                    dificultad = 15;
                }

                if (sumas.isChecked()){
                    tema = "sumas";
                }else if (restas.isChecked()){
                    tema = "restas";
                }else if(multiDiv.isChecked()){
                    tema = "multiDiv";
                }

                nombre = tv.getText().toString();
                i.putExtra("dificultad",dificultad);
                i.putExtra("tema",tema);
                i.putExtra("nombre",nombre);

                if (!(dificultad == 0 || tema == null) ){

                    startActivity(i);
                }else{
                    System.out.println("TIENES QUE ELEGIR LAS OPCIONES");
                }








            }
        });


    }
}