package com.example.examenm8;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Activity2 extends AppCompatActivity {

    String tema;
    int dificultad;
    int contador = 0;
    int contador2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Bundle extras = getIntent().getExtras();
        tema = extras.getString("tema");
        dificultad = extras.getInt("dificultad");
        TextView tv = findViewById(R.id.pregunta);
        Button siguente;
        siguente = findViewById(R.id.enviar);

        List<String> lista;
        lista=new ArrayList();
        for (contador = 0; contador <= dificultad;contador++){
            lista.add("Pregunta numero: "+ contador + " cuanto es 2x2?");
        }
        tv.setText(lista.get(0));

        siguente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(contador2 > dificultad)){
                    contador++;
                    tv.setText(lista.get(contador2));
                }
            }
        });



    }
}